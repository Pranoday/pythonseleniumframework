import pytest
import time
from OrangeHRMDemoApp_Pages.LoginPage import LoginPage
from OrangeHRMDemoApp_Testcases.BaseTestCaseClass import BaseTestCase

#To use CLASS-LEVEL fixture mark class with the fixture name using @pytest.mark.usefixtures("CreateLoginPageObj")
@pytest.mark.usefixtures("CreateLoginPageObj")

class TestLoginFunctionalityWithInvalidCredentials(BaseTestCase):
    loginpage=None
    @pytest.mark.RegressionTest
    def testLoginWithInvalidCredentials(self,LoginDataProvider):
        logger=self.getLogger()
       # loginpage=LoginPage("Chrome")
        #self.DoLogin(LoginDataProvider["UserName","Password"])
        logger.info("Got UserName: "+LoginDataProvider["UserName"])
        logger.info("Got Password: " + LoginDataProvider["Password"])
        TestLoginFunctionalityWithInvalidCredentials.loginpage.T.CreateObjectRepositoy("LoginPage")
        TestLoginFunctionalityWithInvalidCredentials.loginpage.DoLogin(LoginDataProvider["UserName"],LoginDataProvider["Password"])
        time.sleep(3)
        ActualErrorMessage=TestLoginFunctionalityWithInvalidCredentials.loginpage.GetLoginErrorMessage()
        logger.info("Actual Error Message: " + ActualErrorMessage)
        logger.info("Expected Error Message: " + LoginDataProvider["Error"])
        assert ActualErrorMessage==LoginDataProvider["Error"],"Wrong error message is shown"
