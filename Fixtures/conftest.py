import pytest

from OrangeHRMDemoApp_Pages.LoginPage import LoginPage


@pytest.fixture(scope="class")
def OpenApp(request):
    loginpage = LoginPage("Chrome")
    request.cls.loginpage=loginpage